package com.zuitt.activity1;

public class AverageGrade {
    public static void main(String[] args) {
        String firstName = "Juan";
        String lastName = "Dela Cruz";
        float englishGrade = 94;
        float mathGrade = 91;
        float scienceGrade = 89;
        float averageGrade = (englishGrade+mathGrade+scienceGrade)/3;
        System.out.println(firstName + " " + lastName + " " + "your average grade is: " + averageGrade);

    }
}
